Après l'adaptation de la calculette de l'Ademe en ligne de commandes (CLI : Command Line Interface) dans la progression SI4,
nous travaillons sur une version Web, dont les traitements sont effectués en Python
(la version Web écrite en javascript est également disponible dans la progression SI4).

## Objectif

La dernière mission consiste à écrire le script `calcule.py`,
qui été précédemment appelé par le formulaire `calculette.html`.

Le script doit s'appuyer sur le [module] `calculette` (version 5.2) élaboré dans le
cadre de la mission "Gérer les erreurs" de la progression SIO.

Pour cela deux scripts de démonstration sont fournis :
* le script `demo.py` est un "*Hello World*" d'un script Python CGI;
* le script `traite_form_3_1.py` permet de traiter le fichier `formulaire.html`;
* le script `traite_form_3_2.py` améliore la sécurité en protégeant contre
l'injection de code...

[module]: https://framagit.org/ced/CEDy/5-gerer-les-erreurs/-/blob/iteration-5-2/calculette.py
