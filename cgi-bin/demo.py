#!/usr/bin/env python3

print('Content-Type: text/html; charset=utf-8')
print("""
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Hello World</title>
    </head>

    <body>
        <h1>It rocks!</h1>
        <h2>Bonjour le monde</h2>
    </body>
</html>
""")
