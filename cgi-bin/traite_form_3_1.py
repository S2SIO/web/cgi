#! /usr/bin/env python3
import cgitb
import sys
import codecs           # configuration de l'encodage des caractères
import cgi              # Module d'interface avec le serveur Web

cgitb.enable()
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

# Réception de la requête utilisateur :
form = cgi.FieldStorage()       # il s'agit d'une sorte de dictionnaire

if "phrase" in form:
    text = form["phrase"].value
else:
    text = "*** le champ phrase était vide ! ***"

if "visiteur" in form:
    nomv = form["visiteur"].value
else:
    nomv = "mais vous ne m'avez pas indiqué votre nom"

print("Content-Type: text/html; charset=utf-8\n")

print("""
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon formulaire</title>
    </head>

    <body>
        <p>{}</p>
        <h3>Merci, {} !</h3>
        <h4>La phrase que vous m'avez fournie était : {}</h4>
    </body>
</html>
""".format(form.getvalue('visiteur'), nomv, text))
